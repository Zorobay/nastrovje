package nastrovje;

public class Tester {
	
	public static void main(String[] args) {
		
		Complex c1 = new Complex(5, 5);
		Complex c2 = new Complex(5, 5);
		Complex mul = new Complex(7, 3);
		
		System.out.println("Alice: " + c1.mulAlice(mul).toString());
		System.out.println("Bob: " + c2.mulBob(mul).toString());
	}

}
