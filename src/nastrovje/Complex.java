package nastrovje;

public class Complex {
	
	private double re, im;
	
	public  Complex(double re, double im) {
		this.re = re;
		this.im = im;
	}
	
	public Complex mulAlice(Complex c) {
		double newRe = re * c.re - im * c.im;
		im = re * c.im + im * c.re;
		re = newRe;
		return this;
	}
	
	public Complex mulBob(Complex c) {
		double oldRe = re;
		re = re * c.re - im * c.im;
		im = oldRe * c.im + im * c.re;
		return new Complex(re, im);
	}
	
	@Override
	public String toString() {
		return re + ", " + im + "i";
	}

}
